import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

function HatDetail(props) {
    const { hatId } = useParams();
    const navigate = useNavigate();

    const [hat, setHat] = useState(null);


    const fetchHat = async () => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`);
        const data = await response.json();
        setHat(data);
    };
    useEffect(() => {
    fetchHat();
  }, [hatId]);


    if (!hat) {
        return <p>Hat not found</p>
    }
    return(
        <div className='card mx-auto' style={{width: "40rem"}}>
        <img src={hat.picture_url} className='card-img-top' alt="HatIm" style={{objectFit: "cover" }} />
        <div className="card-body text-center">
            <h5 className="card-title">{hat.style} {hat.color}</h5>
            <p className="card-text">Color: {hat.color}</p>
            <p className="card-text">Closet: {hat.location.closet_name}</p>
            <p className="card-text">Section: {hat.location.section_name}</p>
            <p className="card-text">Section: {hat.location.shelf_name}</p>
            <button onClick={() => navigate(-1)} className='btn btn-primary'>Go Back</button>
        </div>
        </div>
    );
}

export default HatDetail;