function ShoeList(props) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Shoe</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {props.shes.map(shoe => {
              return (
                <tr key={shoe.href}>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.bin }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    );
}

export default ShoeList;