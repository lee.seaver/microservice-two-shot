import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatList';
import React, { useEffect, useState } from "react";
import NewHatForm from './NewHatForm';
import HatDetail from './HatDetail';



function App(props) {

  const [hats, setHats] = useState([]);

  const fetchHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats/')

    if (response.ok){
      const data = await response.json();
      setHats(data.hats);
    }else{
      console.error(response)
    }
  }

  useEffect(() => {
    fetchHats();
  }, [])

  const handleCreateHat = (newHat) => {
    setHats([...hats, newHat]);
  }

  if (props.hats === undefined){
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList hats={hats} setHats={setHats} />} />
            <Route path=':hatId' element={<HatDetail hats={hats} setHats={setHats} />} />
            <Route path="new" element={<NewHatForm onCreate={handleCreateHat} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
