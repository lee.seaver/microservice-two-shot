
import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

function HatsList(props) {
    const [hats, setHats] = useState(props.hats);

    const handleDelete = async (hatId) => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, {
            method: 'DELETE',
        });
        if (response.ok) {
            const updatedHats = hats.filter(hat => hat.id !== parseInt(hatId));
            setHats(updatedHats);
        }
    };


    const getData = async () => {
        const hatsUrl = "http://localhost:8090/api/hats/";
        const response = await fetch(hatsUrl);

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="container my-4">
            <h1 className="text-center mb-4">List of Shoes</h1>
            <div className="text-center">
                <NavLink to="/hats/new" className="btn btn-primary">
                    Create New Hat
                </NavLink>
            </div>
            <div className="row">
                {hats.map((hat) => (
                    <div className="col-md-4 mb-4" key={hat.href}>

                        <div className="card">
                            <Link to={`${hat.id}`} style={{ textDecoration: 'none', color: 'black' }} key={hat.id}>
                                <img
                                    src={hat.picture_url}
                                    className="card-img-top"
                                    alt="Hat Image"
                                    style={{ objectFit: "cover" }}
                                />
                            </Link>
                            <div className="card-body text-center">
                                <Link to={`${hat.id}`} style={{ textDecoration: 'none', color: 'black' }} key={hat.id}>
                                    <h5 className="card-title">{hat.manufacturer} {hat.model_name}</h5>
                                    <p className="card-text">Closet Name: {hat.location.closet_name}</p>
                                    <p className="card-text">Shelf Number: {hat.location.section_number}</p>
                                    <p className="card-text">Shelf Number: {hat.location.shelf_number}</p>
                                </Link>
                                <button
                                    type="button"
                                    className="btn btn-danger"
                                    onClick={() => handleDelete(hat.id)}
                                >
                                    Delete
                                </button>
                            </div>
                        </div>

                    </div>
                ))}
            </div>
        </div>
    );
}

export default HatsList;
