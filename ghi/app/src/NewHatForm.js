
import { Link, useNavigate } from 'react-router-dom';
import React, {useEffect, useState} from 'react';

function NewHatForm (props) {
    const navigate = useNavigate();
    const handleSubmit = async (event) => {
        event.preventDefault();

       
        const data = {};

        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;


        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const hatDataResponse = await fetch(hatUrl, fetchConfig);
        if (hatDataResponse.ok) {
            const newHat = await hatDataResponse.json();
            props.onCreate(newHat);

            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
            navigate('/hats');
        } else {
            console.log("*******ERROR. Server response: ", hatDataResponse);
        }
    }

    const [locations, setLocations] = useState([]);


    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');


    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {

        const locationListResponse = await fetch('http://localhost:8100/api/locations/');

        if (locationListResponse.ok) {
            const locationsData = await locationListResponse.json();
            setLocations(locationsData.locations);
        } else {
            console.log("*******ERROR. Server response: ", locationListResponse);
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Create A New Hat</h2>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="mb-3">
                            <label htmlFor="fabric">Fabric</label>
                            <input onChange={handleFabricChange} required type="text" name="fabric" value={fabric} id="fabric" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="style">Style</label>
                            <input onChange={handleStyleNameChange} type="text" id="style" name="style" value={styleName} className="form-control"/>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="color">Color</label>
                            <input onChange={handleColorChange} type="text" id="color" name="color" value={color} className="form-control"/>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="pictureurl" className="form-label">Picture URL</label>
                            <input onChange={handlePictureUrlChange} type="text" className="form-control" name="pictureurl" value={pictureUrl} id="pictureurl" />
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" name="location" value={location} className="form-select">
                            <option value="">Choose a closet</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.href}>
                                        {location.closet_name}: Section {location.section_number}, Shelf {location.shelf_number}
                                    </option>
                                )
                            })
                            }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewHatForm;

